package cafes.domain;

import cafes.domain.Address;
import cafes.domain.restService.footballMatch.FootballMatch;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-10T23:21:32")
@StaticMetamodel(Cafe.class)
public class Cafe_ { 

    public static volatile ListAttribute<Cafe, FootballMatch> nextMatchesToWatch;
    public static volatile SingularAttribute<Cafe, Address> address;
    public static volatile SingularAttribute<Cafe, String> name;
    public static volatile SingularAttribute<Cafe, String> favouriteTeam;
    public static volatile SingularAttribute<Cafe, Long> cafeID;

}