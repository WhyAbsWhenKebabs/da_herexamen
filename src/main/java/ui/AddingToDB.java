package ui;

import cafes.domain.Address;
import cafes.domain.Cafe;
import cafes.domain.restService.footballMatch.FootballMatch;
import java.util.List;
import javax.inject.Inject;
import service.CafeFacade;

/**
 *
 * @author Tom
 */
public class AddingToDB {
    
    /*public static final long TESTED_FOOTBALLMATCH_ID = 99;
    
    @Inject
    private CafeFacade cafeFacade;
    
    public AddingToDB() {
        // clearing DB for proper using
        this.cafeFacade.clear();
    }
    
    public void addCafes(){
        // create an address for our first cafe
        Address address1 = new Address("Groenstraat", "Zaventem", 38, 1930);
        // create an address for our second cafe
        Address address2 = new Address("Hoogstraat", "Zaventem", 55, 1930);
        
        // create our first cafe
        Cafe cafe1 = new Cafe("Volkshuis", address1, "Anderlecht");
        // create our second cafe
        Cafe cafe2 = new Cafe("Hof ter Meren", address2, "Brugge");
        
        // simulate footballMatch data received from a rest service
        FootballMatch footballMatch = new FootballMatch("Anderlecht", "Standard");
        footballMatch.setFootballMatchID(new Long(TESTED_FOOTBALLMATCH_ID));
        
        // adding football match to our first cafe
        cafe1.addNextMatchToWatch(footballMatch);
        
        // adding both cafes to our database
        cafeFacade.addCafe(cafe1);        
        cafeFacade.addCafe(cafe2);
    }
    
    public List<Cafe> getCafes(){
        return this.cafeFacade.getCafes();
    }
    
    // Some small test ..
    public String findCafesSendingOutSpecificMatch(FootballMatch footballMatch){
        List<Cafe> cafes = cafeFacade.listOfCafesSendingOutSpecificFootballMatch(footballMatch.getFootballMatchID());
        String result = "";
        int counter = 1;
        for(Cafe cafe: cafes){
            result = result + counter + ".) " + cafe + "\n";
            counter++;
        }
        return result;
    }
    */
    
}
