package cafes.db;

import cafes.domain.Address;
import cafes.domain.Cafe;
import cafes.domain.restService.footballMatch.FootballMatch;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;

/**
 *
 * Originally this stub class was used for testing withour using a db yet
 * 
 * @author Tom
 */
@RequestScoped
@Alternative
public class CafeStub implements CafeRepository {
    
    private static Map<Long, Cafe> cafes;
    private static long counter;

    public CafeStub() {
        cafes = new HashMap<Long, Cafe>();
        
        counter = 0;
        
        // adding some dara
        this.addSomeData();
    }

    @Override
    public long addCafe(Cafe cafe) throws ValidationException{
        if(cafe == null){
            throw new ValidationException("No cafe given");
        }
        counter++;
        cafe.setCafeID(counter);
        cafes.put(cafe.getCafeID(), cafe);
        return cafe.getCafeID();
    }

    @Override
    public Cafe getCafe(long cafeID) throws ValidationException, DBException{
        if(cafeID < 0){
            throw new ValidationException("No correct cafeID given");
        }
        if(!cafes.containsKey(cafeID)){
            throw new DBException("No cafe was found");
        }
        return cafes.get(cafeID);
    }
    
    @Override
    public List<Cafe> getCafes(){
        List<Cafe> list = new ArrayList<Cafe>();
        for(Cafe cafe: cafes.values()){
            list.add(cafe);
        }
        return list;
    }

    @Override
    public void deleteCafe(long cafeID) throws DBException{
        if(!cafes.containsKey(cafeID)){
            throw new DBException("No cafe was found");
        }
        cafes.remove(cafeID);
    }

    @Override
    public void updateCafe(Cafe cafe) throws ValidationException, DBException{
        if(cafe == null){
            throw new ValidationException("No cafe given");
        }
        if(!cafes.containsKey(cafe.getCafeID())){
            throw new DBException("No cafe was found");
        }
        cafes.put(cafe.getCafeID(), cafe);
    }

    @Override
    public List<Cafe> listOfCafesSendingOutSpecificFootballMatch(Long footballMatch) {
        List<Cafe> result = new ArrayList<Cafe>();
        for(Cafe cafe: cafes.values()){
            if(cafe.isSendingOutSpecificFootballMatch(footballMatch)){
                result.add(cafe);
            }
        }
        return result;
    }
    
    private void addSomeData(){
        // create an address for our first cafe
        Address address1 = new Address("Groenstraat", "Zaventem", 38, 1930);
        // create an address for our second cafe
        Address address2 = new Address("Hoogstraat", "Zaventem", 55, 1930);
        
        // create our first cafe
        Cafe cafe1 = new Cafe("Volkshuis", address1, "Anderlecht");
        // create our second cafe
        Cafe cafe2 = new Cafe("Hof ter Meren", address2, "Brugge");
        
        // simulate footballMatch data received from a rest service
        FootballMatch footballMatch = new FootballMatch("Anderlecht", "Standard");
        footballMatch.setFootballMatchID(new Long(99));
        
        // adding football match to our first cafe
        cafe1.addNextMatchToWatch(footballMatch);
        try{
            // adding both cafes to our database
            this.addCafe(cafe1);        
            this.addCafe(cafe2);
        }catch(ValidationException ex){
            System.out.println(ex.getMessage() + " on stub for testing ...");
        }
    }
    
}
