package cafes.db;

import cafes.domain.Cafe;
import cafes.domain.restService.footballMatch.FootballMatch;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

/**
 * 
 * @author Tom
 */
@RequestScoped
@Default
public class CafeDB implements CafeRepository {    
    
    @PersistenceContext(unitName = "CafesPU")
    private EntityManager manager;
    
    @Resource
    private Validator validator;

    public CafeDB() {

    }

    public EntityManager getManager() {
        return manager;
    }

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }   

    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }
    
    private void checkConstraints(Object object) throws ValidationException{
        Set<ConstraintViolation<Object>> constraintsAdress = this.getValidator().validate(object);
        if(!constraintsAdress.isEmpty()){
            String fullErrorConstraint = "";
            for (ConstraintViolation<Object> constraint : constraintsAdress) {
                fullErrorConstraint = fullErrorConstraint + constraint.getMessage() + "\n";
            }
            throw new ValidationException(fullErrorConstraint);
        }
    }
    
    @Transactional
    @Override
    public long addCafe(Cafe cafe) throws ValidationException, DBException{
        this.checkConstraints(cafe.getAddress());
        for(FootballMatch footballMatch: cafe.getNextMatchesToWatch()){
            this.checkConstraints(footballMatch);
        }
        this.checkConstraints(cafe);
        this.getManager().persist(cafe);
        this.manager.flush();
        return cafe.getCafeID();
    }

    @Override
    public Cafe getCafe(long cafeID) throws ValidationException, DBException {
        if(cafeID < 0 || cafeID > Long.MAX_VALUE){
            throw new ValidationException("Invalid cafeID given");
        }
        Cafe cafe = this.getManager().find(Cafe.class, cafeID);
        if(cafe == null){
            throw new DBException("No cafe exist with given cafeID");
        }
        return cafe;
    }
    
    @Override
    public List<Cafe> getCafes(){
        String queryString = "SELECT c FROM Cafe c";            
        Query query = this.getManager().createQuery(queryString);
        return query.getResultList();
    }
    
    @Transactional
    @Override
    public void deleteCafe(long cafeID) throws ValidationException, DBException{
        Cafe cafe = this.getCafe(cafeID);
        Cafe toBeRemoved = manager.merge(cafe);
        manager.remove(toBeRemoved);
        manager.flush();
        manager.clear();
    }
    
    @Transactional
    @Override
    public void updateCafe(Cafe cafe) throws ValidationException, DBException{
        this.getCafe(cafe.getCafeID()); // check if cafe exists
        this.checkConstraints(cafe.getAddress());
        for(FootballMatch footballMatch: cafe.getNextMatchesToWatch()){
            this.checkConstraints(footballMatch);
        }
        this.checkConstraints(cafe);
        this.getManager().merge(cafe);
        this.manager.flush();
    }

    @Override
    public List<Cafe> listOfCafesSendingOutSpecificFootballMatch(Long footballMatch) throws ValidationException{
        if(footballMatch < 0 || footballMatch > Long.MAX_VALUE){
            throw new ValidationException("Invalida footballMatch given");
        }
        List<Cafe> result = new ArrayList<Cafe>();
        for(Cafe cafe: this.getCafes()){
            if(cafe.isSendingOutSpecificFootballMatch(footballMatch)){
                result.add(cafe);
            }
        }
        return result;
    }
    
}
