package cafes.db;

import cafes.domain.Cafe;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Tom
 */
@Local
public interface CafeRepository {
    
    long addCafe(Cafe cafe) throws ValidationException, DBException;
    Cafe getCafe(long cafeID) throws ValidationException, DBException;
    List<Cafe> getCafes();
    void deleteCafe(long cafeID) throws ValidationException, DBException;
    void updateCafe(Cafe cafe) throws ValidationException, DBException;
    List<Cafe> listOfCafesSendingOutSpecificFootballMatch(Long footballMatch) throws ValidationException;
}
