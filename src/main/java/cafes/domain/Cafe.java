package cafes.domain;

import cafes.domain.restService.footballMatch.FootballMatch;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Tom
 */
@Entity
public class Cafe implements Serializable {
    
    @Column(name = "name", nullable = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Pattern(regexp = "[a-zA-Z]{1,30}")
    private String name;
    
    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.EAGER)
    @JoinColumn(name = "address", nullable = false, unique = true)
    @NotNull
    private Address address;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cafe_id", updatable = false, nullable = false)
    private Long cafeID;
    
    @Column(name = "favourite_team", nullable = true)
    @Size(min = 0, max = 30)
    @Pattern(regexp = "[a-zA-Z]{0,30}")
    private String favouriteTeam;
    
    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.EAGER)
    private List<FootballMatch> nextMatchesToWatch;

    public Cafe() {
        this.nextMatchesToWatch = new ArrayList<FootballMatch>();
    }
    
    public Cafe(String name, Address address, String favouriteTeam){
        this.nextMatchesToWatch = new ArrayList<FootballMatch>();
        this.setName(name);
        this.setAddress(address);
        this.setFavouriteTeam(favouriteTeam);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCafeID() {
        return cafeID;
    }

    public void setCafeID(Long cafeID) {
        this.cafeID = cafeID;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getFavouriteTeam() {
        return favouriteTeam;
    }

    public void setFavouriteTeam(String favouriteTeam) {
        this.favouriteTeam = favouriteTeam;
    }

    public List<FootballMatch> getNextMatchesToWatch() {
        return nextMatchesToWatch;
    }

    public void addNextMatchToWatch(FootballMatch nextMatchToWatch) {
        this.nextMatchesToWatch.add(nextMatchToWatch);
    }
    
    public boolean isSendingOutSpecificFootballMatch(Long footballMatchID){
        for(FootballMatch footballMatch: this.getNextMatchesToWatch()){
            if(Objects.equals(footballMatch.getFootballMatchID(), footballMatchID)){
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String toString(){
        return "Cafe " + this.getName() + " with following address: " + this.getAddress() + " and favourite team: " + this.getFavouriteTeam();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.cafeID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cafe other = (Cafe) obj;
        if (!Objects.equals(this.cafeID, other.cafeID)) {
            return false;
        }
        return true;
    }
    
}
