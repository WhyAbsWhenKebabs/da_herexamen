package cafes.domain.restService.footballMatch;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * 
 * This will be the data recovered from a restService
 * We only save the ID and te Home and Awayteam of the Football match
 * when a user wants to knows more about the match we call the rest service given an id of the specific football match
 * 
 * @author Tom
 */

@Entity
public class FootballMatch implements Serializable {
    
    @Id
    @NotNull
    @Column(name = "footballMatch_id", updatable = false, nullable = false)
    private Long footballMatchID;
    
    @Column(name = "away_team", nullable = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Pattern(regexp = "[a-zA-Z]*")
    private String awayTeam;
    
    @Column(name = "home_team", nullable = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Pattern(regexp = "[a-zA-Z]*")
    private String homeTeam;
    
    @Transient
    @JsonIgnore
    private Date dateOfMatch;
    
    @Transient
    @JsonIgnore
    private String odd;
    
    @Transient
    @JsonIgnore
    private String lastConfrontation;

    public FootballMatch() {
        
    }

    public FootballMatch(String awayTeam, String homeTeam) {
        this.setAwayTeam(awayTeam);
        this.setHomeTeam(homeTeam);
    }   

    public Long getFootballMatchID() {
        return footballMatchID;
    }

    public void setFootballMatchID(Long footballMatchID) {
        this.footballMatchID = footballMatchID;
    }

    public String getAwayTeam() {
        return awayTeam;
    }
    
    @JsonProperty
    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public String getHomeTeam() {
        return homeTeam;
    }
    
    @JsonProperty
    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }
    
    @JsonIgnore
    public Date getDateOfMatch() {
        return dateOfMatch;
    }
    
    @JsonProperty
    public void setDateOfMatch(Date dateOfMatch) {
        this.dateOfMatch = dateOfMatch;
    }
    
    @JsonIgnore
    public String getOdd() {
        return odd;
    }

    @JsonProperty
    public void setOdd(String odd) {
        this.odd = odd;
    }
    
    @JsonIgnore
    public String getLastConfrontation() {
        return lastConfrontation;
    }
    
    @JsonProperty
    public void setLastConfrontation(String lastConfrontation) {
        this.lastConfrontation = lastConfrontation;
    }
    
    public String toString(){
        return this.getHomeTeam() + " - " + this.getAwayTeam();
    }
}
