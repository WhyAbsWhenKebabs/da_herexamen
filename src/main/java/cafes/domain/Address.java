package cafes.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Tom
 */
@Entity
public class Address implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "address_id", updatable = false, nullable = false)
    private Long addressID;
    
    @Column(name = "street", nullable = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Pattern(regexp = "[a-zA-Z]{1,30}")
    private String street;
    
    @Column(name = "city", nullable = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Pattern(regexp = "[a-zA-Z]{1,30}")
    private String city;
    
    @Column(name = "number", nullable = false)
    @NotNull
    @Min(1)
    @Max(999)
    private Integer number;
    
    @Column(name = "postcode", nullable = false, length = 4)
    @NotNull
    @Min(1000)
    @Max(9999)
    private Integer postcode;

    public Address() {
        
    }

    public Address(String street, String city, int number, int postcode) {
        this.setStreet(street);
        this.setCity(city);
        this.setNumber(number);
        this.setPostcode(postcode);
    }   

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.addressID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Address other = (Address) obj;
        if (!Objects.equals(this.addressID, other.addressID)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
        return this.getCity() + " " + this.getPostcode() + " " + this.getStreet() + " " + this.getNumber();
    }   
    
}
