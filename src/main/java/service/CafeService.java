package service;

import cafes.db.CafeRepository;
import cafes.db.DBException;
import cafes.db.ValidationException;
import cafes.domain.Cafe;
import cafes.domain.restService.footballMatch.FootballMatch;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

/**
 *
 * CafeService - Service class *  
 * 
 * @author Tom
 */
@Stateless
@Dependent
public class CafeService implements CafeFacade{

    @Inject
    private CafeRepository cafeRepository;

    public CafeService() {
        
    }

    @Override
    public long addCafe(Cafe cafe) throws ValidationException, DBException {
        return this.cafeRepository.addCafe(cafe);
    }

    @Override
    public Cafe getCafe(long cafeID) throws ValidationException, DBException{
        return this.cafeRepository.getCafe(cafeID);
    }
    
    @Override
    public List<Cafe> getCafes(){
        return this.cafeRepository.getCafes();
    }

    @Override
    public void deleteCafe(long cafeID) throws ValidationException, DBException {
        this.cafeRepository.deleteCafe(cafeID);
    }

    @Override
    public void updateCafe(Cafe cafe) throws ValidationException, DBException{
        this.cafeRepository.updateCafe(cafe);
    }

    @Override
    public List<Cafe> listOfCafesSendingOutSpecificFootballMatch(Long footballMatch) throws ValidationException{
        return this.cafeRepository.listOfCafesSendingOutSpecificFootballMatch(footballMatch);
    }
    
    @Override
    public void addExtraMatchToNextMatchesToView(FootballMatch footballMatch, long cafeID) throws ValidationException, DBException{
        Cafe cafe = this.cafeRepository.getCafe(cafeID);
        cafe.addNextMatchToWatch(footballMatch);
        this.updateCafe(cafe);        
    }
    
    
}
