package service;

import cafes.db.DBException;
import cafes.db.ValidationException;
import cafes.domain.Cafe;
import cafes.domain.restService.footballMatch.FootballMatch;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Tom
 */
@Local
public interface CafeFacade {
    
    long addCafe(Cafe cafe) throws ValidationException, DBException;
    Cafe getCafe(long cafeID) throws ValidationException, DBException;
    List<Cafe> getCafes();
    void deleteCafe(long cafeID) throws ValidationException, DBException;
    void updateCafe(Cafe cafe) throws ValidationException, DBException;
    List<Cafe> listOfCafesSendingOutSpecificFootballMatch(Long footballMatch) throws ValidationException;
    void addExtraMatchToNextMatchesToView(FootballMatch footballMatch, long cafeID) throws ValidationException, DBException;
    
}
